<?php
declare(strict_types=1);

namespace App\Controller;

use App\Services\Rot13Transformer;
use App\Services\TransformerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
//    private $tranformer;
//
//    public function __construct(Rot13Transformer $transformer)
//    {
//        $this->tranformer = $transformer;
//    }

    /**
     * @Route(
     *     "/"
     * )
     * @return Response
     */
    public function test(TransformerInterface $rot13Transformer)
    {
        return new Response($rot13Transformer->transform('Hello world!1122'));
    }
}
