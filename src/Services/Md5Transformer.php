<?php
declare(strict_types=1);

namespace App\Services;


class Md5Transformer implements TransformerInterface
{
    public function transform($value)
    {
        return md5($value);
    }
}
