<?php
declare(strict_types=1);

namespace App\Services;

class Rot13Transformer implements TransformerInterface
{
    private $prefix;

    public function __construct($prefix = '')
    {
        $this->prefix = $prefix;
    }

    public function transform($value)
    {
        return $this->prefix . str_rot13($value);
    }
}
